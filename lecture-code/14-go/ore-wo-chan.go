// go example - ore smelting with simple style - without channels
// TODO convert this program to use channels.
// Note: you could do away with oreList and minedOreList, etc. and use channels instead.
package main

import (
	"fmt"
	//"time" // use this for time inserting in the channel version
)

// ore finder
func finder(mine [5]string) []string{
	fmt.Println("Started finder function.")
	var oreList = []string{}
	for _, item := range mine {
        if item == "ore" {
			fmt.Println("Found: ", item)
			oreList = append(oreList, item) //TODO: for channels, insted of this, write to oreChannel
        }
    }
	return oreList// this returns oreList
} // end of finder

// Ore Breaker
func oreBreaker(oreList []string) []string {
	fmt.Println("Started oreBreaker function.")
	var minedOreList = []string{}
	for i := 0; i < 3; i++ {
		foundOre := oreList[i] //TODO: for channels, insted of this, read from oreChannel
		fmt.Println("Breaking: ", foundOre)
		minedOreList = append(minedOreList, "minedOre") //TODO: for channels, insted of this, send to minedOreChan
	}
	return minedOreList
} // end of ore breaker

// Smelter
func smelter(minedOreList []string) {
	fmt.Println("Started smelter function.")
	for i := 0; i < 3; i++ {
		minedOre := minedOreList[i] //TODO: for channels, insted of this, read from minedOreChan
		fmt.Println("Smelting: ", minedOre)
	}
} // end of smelter

func main() {
	theMine := [5]string{"rock", "ore", "ore", "rock", "ore"}

	oreList := finder(theMine)
	minedOreList := oreBreaker(oreList)
	smelter(minedOreList)

	//<-time.After(time.Second * 1) // you will use this in the channel version
} // end of main

// output of this program is:
// Found:  ore
// Found:  ore
// Found:  ore
// Breaking:  ore
// Breaking:  ore
// Breaking:  ore
// From Smelter:  minedOre
// From Smelter:  minedOre
// From Smelter:  minedOre
// we notice how everything functions sequentially.

