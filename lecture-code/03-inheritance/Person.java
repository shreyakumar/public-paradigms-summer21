// Better example using inheritance
import java.util.*;

public class Person{
    // properties
    String netid;
    String firstName;
    String lastName;

    // constructor
    Person(String netid, String firstName, String lastName){
        this.netid      = netid;
        this.firstName  = firstName;
        this.lastName   = lastName;
    } // end of constructor

    // behavior
    String findFood(){
        return "go to Duncan Center";
    }

    // getters and setters
    String getNetid(){    return this.netid;        }
    void setNetid(String id){   this.netid = id;    }
    String getFirstName(){    return this.firstName;        }
    void setFirstName(String name){   this.firstName = name;    }
    String getLastName(){    return this.lastName;        }
    void setLastName(String name){   this.lastName = name;    }

    // main
    public static void main(String[] args){
        Person p = new Person("a1", "Dwight", "Shrute");
        Faculty f1 = new Faculty("a2", "Dean", "Shrute", 100);
        Student s1 = new Student("z3", "Donna", "Shrute", 3.9);

        System.out.println("Food: " + p.findFood());

        System.out.println("Salary: " + f1.calculateSalary());
        System.out.println("Fin Aid: " + s1.calculateFinancialAid());


    }
} // end of person

class Student extends Person{ // Student is a child class of Person(parent)

    double gpa;

    // constructor
    Student(String netid, String firstName, String lastName, double gpa){
        super(netid, firstName, lastName); // calls Person(netid, fname, lname) constructor
        this.gpa = gpa;
    } // end of constructor

    // behavior
    double calculateFinancialAid(){
        return 500000;
    }

    // getters and setters
    double getGpa(){    return this.gpa;        }
    void setGpa(double gpa){   this.gpa = gpa;    }


} // end of Student class

class Faculty extends Person{
    double salary;

    // constructor
    Faculty(String netid, String firstName, String lastName, double salary){
        super(netid, firstName, lastName); // calls Person(netid, fname, lname) constructor
        this.salary = salary;
    } // end of constructor

    // behavior
    double calculateSalary(){
       return 100000;
    }

    // getters and setters
    double getSalary(){    return this.salary;        }
    void setSalary(double salary){   this.salary = salary;    }

} // end of Faculty class
