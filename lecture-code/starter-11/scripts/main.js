console.log("page loaded!")

// connect button listener
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo.');
    // get name from user
    var name = document.getElementById('name-text').value;
    console.log(name);
    // make nw call to age api with the user name
    makeNetworkCallToAgeApi(name)
} // end of getFormInfo

function makeNetworkCallToAgeApi(name){
    console.log('got name = ' + name);
    var url = "https://api.agify.io/?name=" + name;

    var xhr = new XMLHttpRequest(); // 1. create xhr object
    xhr.open("GET", url, true); // 2. configure the object

    xhr.onload = function(e){
        console.log('age api onload triggered');
        console.log('network response received = ' + xhr.responseText);
        updateAgeWithResponse(name, xhr.responseText);
    } // end of xhr.onload

    xhr.onerror = function(e){
        console.log('age api onerror triggered' + xhr.statusText);
    } // end of xhr.onerror

    xhr.send(null); // this actually sends the request
} // end of makeNetworkCallToAgeApi

function updateAgeWithResponse(name, response_text){
    // extract age information from json response
    var response_json = JSON.parse(response_text);

    // update label with it
    var label1 = document.getElementById('result-line1');
    label1.innerHTML = response_json['age'];
    // and make nw call to numbers api
    //makeNetworkCallToNumbersApi()
}

function makeNetworkCallToNumbersApi(){
    // TODO
    // get response
    // update UI
} // end of makeNetworkCallToNumbersApi
